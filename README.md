#DoodleDemo

Welcome to Doodle Polls testing demo. 

This is a simple Regression Autotest framework which can be used to simulate and test creation and making a choice in Doodle Polls.

Framework for this demo was built from scratch, without any reusage of code.
Access to this framework is public so anyone who wishes can download this code and test it for themselves. Please do bear in mind that I am using my own e-mail in regression, so try to keep running these tests in number less than 1000. :) 
I would you like to review the code and application and to share your thoughts with me. I would appreciate any suggestion. If you have any question please contact me.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

What is needed to run this? 

1) GRADLE - version 4.8 (or above)
2) Java JDK 1.8 (or above)

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

How to build and execute this project?

1) Using GRADLE (from command line/powershell etc.):  
gradle clean build - (to build and run for the first time)
gradle test --tests *SomeTestClass - (to execute some specific TestClass)
gradle test --tests *SomeTestClass.SomeTestCase - (to execute a test case inside of specific TestClass)
Windows specific hint: You can open up powershell directly at needed location by holding shift and right-clicking on desired folder containing the project.

2) Using IntelliJ/Eclipse/etc.:
open new project, import settings.gradle file, and check auto-import option. Be sure to set valid path for both the gradle and the jdk. Simply Build and Run after that.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

What are the technologies used in this project?

Code is Written in Java. Gradle with Maven is used for fetching needed jars/libs. 
Both the Selenium WebDriver and Standalone server are running on version 3.13. 
For testing purposes JUnith 4.12 is used on this occassion. 
Most of the searches of WebElements were done using XPaths, as it is the most powerful option, with widest variety of options. 

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

What is the structure of the project?

src is containing the folders: main & test.
MAIN:
1)Packages Browser, Pages, Utils
	i)  Browser
		 - Class Browser;
		 Short Summary: wrapper for RemoteWebDriver, added custom methods for waiting WebElements to be visible, implementing actions for setting mouse, double clicking, getting attributes, etc...
	ii) Utils
		 - Class Time;
		 Short summary: support methods, for getting current day, date and time, as well as remaining dates in the month, or random number of remaining dates in the month.
	iii)Pages
		 - Class DoodleMainPage;
		 - Class DoodlePollOccasionPage;
		 - Class DoodleOptionCalendar;
		 - Class DoodlePollSettings;
		 - Class DoodleIntroduceToParticipants;
		 - Class DoodlePollPage;
		 Short summary: Each class contains support methods for each WebPage on which any action is needed to be done. Certain support methods have not been implemented to the end because the task would last much longer than 3-4h of dev time. 
		 Good example is Calendar page, in which I decided to create support method for choosing and random number of remaining days in the month, and adding times to them. 
TEST: 
1) Packages InitialChecks, DoodleTask
	i)  InitialChecks
		 - Class GoogleSearchTest;
		 Short summary: Class contains 2 simple tests, for basic checking of framework functionalities, used for starting point for creation of Browser class. 
		 a) test_01_openGoogleHomepage - Checking if logo is displayed
		 b) test_02_googleSearchDoodle - Opening Google.com and reaching Doodle.com through GoogleSearch. Used for testing waiting Visibility, Clicking etc...
	ii) DoodleTask
		 - Class DoodleTaskPollsTest;
		 Short summary: Class contains 5 autotests, of which 2 are used for checking process of creating polls, and 3 are currently on Ignore, which were used for checking singleClick(), doubleClick() and coloring of columns in already created single and multiple column polls.
		 a) !MAIN TEST! test_01_DoodlePollMainTest - checking if after successfully creating a poll, selecting "Yes, if need be" option will mark it as that and color the column in orange. 
		 b) test_02_DoodlePollTestExpectedStopOnStep1 - Test is checking if Continue button is correctly disabled if no name of Poll is provided.
		 c) test_10_checkOfColumnAndCheckboxSingleClick (@Ignore) - Testing if single click is working with selecting option Yes, and coloring column green.
		 d) test_11_checkOfColumnAndCheckboxDoubleClick (@Ignore) - Testing if double click is working with selecting option Yes, if need be while coloring column in orange.
		 e) test_20_testingOfExistingPollPage (@Ignore) - Test Helper for checking changes in coloring background.

		 If you want to run Ignored tests, please remove @Ignore annotations above those cases.
		 

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

What could be improved? 

Extend support methods for DoodleOptionCalendar class;
Switching from exclusive page based approach, to mix with Component based approach to support methods. Through most of the pages, same button for "Continue" was used. 
Using Component Approach, we would be able to just send that Class a method containing current path to it, and do both the checks and actions from there.
Create a wrapper of WebElement in which there would be reimplementation of "By", to which we would give parameters of type of search and path to search. Benefit would be cleaner code.
Implementation in Browser, of throwing custom Exceptions, when items are not Visible or Present when expected to be. 
Add option for more browser types.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Special note(s) :) 

I would not be a QE, if I would not find some bug while manually exploring options. This are the cases I do not think it should happen:

I)  On choosing dates in Calendar for options, it is not possible to choose dates that are in the past, which is totally understandable.
	But, if we choose optional way, to add specific times to each date, we ARE allowed to choose times that has already passed, at both the start and finish. 
	This should not be allowed, because, if there is limit on dates that cannot be created for, so the checks would need to happen for time of current day as well.
	
II) Smaller one, but still. Also when adding times, we can add multiple times for 1 day. For instance, we add 3 days, and set times for all three days. When we click on (x) for 3rd day, that will remove that selected date, from the calendar, as well as the time option.
	But it will also remove from UI all set date times but the first date times. Only when clicking again on "Need different times for each day?" it will show those already selected options in the UI.
	For me, all selected times for days except the date that was cancelled should remain visible on UI.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Thank you for your time and attention. 

Kind regards, Marko Vukasinovic.