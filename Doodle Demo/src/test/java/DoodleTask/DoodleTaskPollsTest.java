package DoodleTask;

import Browser.Browser;
import Pages.*;
import Utils.Time;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DoodleTaskPollsTest {
    private static Browser browser;
    private static Time time;
    private static DoodleMainPage doodleMainPage;
    private static DoodlePollOccasionPage doodleOccassionPage;
    private static DoodleOptionCalendar doodleOptionCalendar;
    private static DoodlePollSettings doodlePollSettings;
    private static DoodleIntroduceToParticipants doodleIntroduceToParticipants;
    private static DoodlePollPage doodlePollPage;

    @BeforeClass
    public static void initClass(){
        System.out.println("Started Execution of all test cases.");
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        time = new Time();
    }

    @Before
    public void resetBrowser(){
        browser = new Browser();
    }

    /** Test is checking if after successfully creating a poll, selecting "Yes, if need be" option will mark it as that and color the column in orange. */
    @Test
    public void test_01_DoodlePollMainTest() throws Exception {

        doodleMainPage = new DoodleMainPage(browser);
        doodleOccassionPage = new DoodlePollOccasionPage(browser);
        doodleOptionCalendar = new DoodleOptionCalendar(browser);
        doodlePollSettings = new DoodlePollSettings(browser);
        doodleIntroduceToParticipants = new DoodleIntroduceToParticipants(browser);
        doodlePollPage = new DoodlePollPage(browser);

        doodleMainPage.enterDoodleMainPage();
        Assert.assertEquals("Get together with Doodle", doodleMainPage.getDoodleWelcomeMessage());
        doodleMainPage.clickOnDoodlePollButton();
        browser.sleep(1);

        Assert.assertEquals(true, doodleOccassionPage.enteredPollOccassionPage());
        Assert.assertEquals("What’s the occasion?", doodleOccassionPage.getWhatIsOccassion());

        String occassionTitle = "Doodle Demo Test";
        String occassionLocation = "Belgrade";
        String[] occassionNotes = {"Autotest", "Testing", "Multiple", "Rows", "Input"};

        Assert.assertEquals(true,doodleOccassionPage.checkInitOccassionForm());
        doodleOccassionPage.fillOccassionForm(occassionTitle, occassionLocation, occassionNotes);
        Assert.assertEquals(true,doodleOccassionPage.getContinueButtonStatus());
        doodleOccassionPage.clickOnContinue();
        browser.sleep(1);

        Assert.assertEquals(true, doodleOptionCalendar.enteredCalendarPage());
        Assert.assertEquals("What are the options?", doodleOptionCalendar.getOptions());

        /** Decided to go for selecting random days in calendar and adding of timeframe */
        Assert.assertEquals("Month", doodleOptionCalendar.SelectOption("Month"));
        int selectNumberOfDays = 1 + (int)(Math.random() * ((time.getRemainingNumberOfDays() - 1) + 1));
        Assert.assertEquals(false, doodleOptionCalendar.getContinueButtonStatus());
        doodleOptionCalendar.selectCalendarDates(time.getRandomDaysUntilEndOfMonth(selectNumberOfDays), true, "10:00 PM", "11:22 PM"); //Just an example of what my code can do.
        Assert.assertEquals(true, doodleOptionCalendar.getContinueButtonStatus());
        doodleOptionCalendar.clickOnContinue();
        browser.sleep(1);

        Assert.assertEquals(true, doodlePollSettings.enteredPollSettingsPage());
        Assert.assertEquals("Poll settings", doodlePollSettings.getPollSettings());
        doodlePollSettings.selectPollOptions(true, false, false);
        Assert.assertEquals(true, doodleOptionCalendar.getContinueButtonStatus());
        doodlePollSettings.clickOnContinue();
        browser.sleep(1);

        Assert.assertEquals(true, doodleIntroduceToParticipants.enteredIntroduce());
        Assert.assertEquals("Tell your participants who you are", doodleIntroduceToParticipants.getIntroduce());
        Assert.assertEquals("Finish", doodleIntroduceToParticipants.buttonContinueText());
        doodleIntroduceToParticipants.enterUserData("VukshaCage", "marko.vukashinovic@gmail.com");
        doodleIntroduceToParticipants.clickOnContinue();
        browser.sleep(1);

        doodlePollPage.setUrlPath();
        System.out.println(doodlePollPage.getUrlPath());
        Assert.assertEquals("#FCF6C5", doodlePollPage.checkDoubleClick());

        System.out.println("Test Finished.");
    }

    /** Test is checking if Continue button is correctly disabled if no name of Poll is provided. */
    @Test
    public void test_02_DoodlePollTestExpectedStopOnStep1() throws Exception {

        doodleMainPage = new DoodleMainPage(browser);
        doodleOccassionPage = new DoodlePollOccasionPage(browser);

        doodleMainPage.enterDoodleMainPage();
        Assert.assertEquals("Get together with Doodle", doodleMainPage.getDoodleWelcomeMessage());
        doodleMainPage.clickOnDoodlePollButton();
        browser.sleep(5);

        Assert.assertEquals(true, doodleOccassionPage.enteredPollOccassionPage());
        Assert.assertEquals("What’s the occasion?", doodleOccassionPage.getWhatIsOccassion());

        String occassionTitle = "";
        String occassionLocation = "Belgrade";
        String[] occassionNotes = {"Autotest", "Testing", "Multiple", "Rows", "Input"};

        Assert.assertEquals(true,doodleOccassionPage.checkInitOccassionForm());
        doodleOccassionPage.fillOccassionForm(occassionTitle, occassionLocation, occassionNotes);
        Assert.assertEquals(false,doodleOccassionPage.getContinueButtonStatus());

        System.out.println("Test Finished.");

    }

    /** Testing if single click is working with selecting option Yes, and coloring column green. */
    @Ignore
    @Test
    public void test_10_checkOfColumnAndCheckboxSingleClick() throws Exception {
        String pollURL = "https://doodle.com/poll/x4wr48nnrr6mqeir";
        browser.goTo(pollURL);
        browser.sleep(1);

        doodlePollPage = new DoodlePollPage(browser);
        Assert.assertEquals("#EBF7D4", doodlePollPage.checkClick());
    }

    /** Testing if double click is working with selecting option Yes, if need be while coloring column in orange. */
    @Ignore
    @Test
    public void test_11_checkOfColumnAndCheckboxDoubleClick() throws Exception {
        String pollURL = "https://doodle.com/poll/kwy96v3xcmsniyvg";
        browser.goTo(pollURL);
        browser.sleep(1);

        doodlePollPage = new DoodlePollPage(browser);
        Assert.assertEquals("#FCF6C5", doodlePollPage.checkDoubleClick());
    }

    /** Test Helper for checking changes in coloring background. */
    @Ignore
    @Test
    public void test_20_testingOfExistingPollPage() throws Exception {
        String pollURL = "https://doodle.com/poll/pyv43xxtwmvg22fn";
        WebElement backgroundColor;
        browser.goTo(pollURL);
        browser.sleep(1);

        backgroundColor = browser.findElement(By.xpath("//*[@id=\"d-pollView\"]/article/div/div/div/ul/li"));
        System.out.println(backgroundColor.getCssValue("background-color"));
        browser.click(By.xpath("//*[@class=\"d-defaultCheckmark\"]"));
        browser.sleep(1);

        backgroundColor = browser.findElement(By.xpath("//*[@id=\"d-pollView\"]/article/div/div/div/ul/li"));
        System.out.println(backgroundColor.getCssValue("background-color"));
        browser.click(By.xpath("//*[@class=\"d-defaultCheckmark\"]"));
        browser.sleep(1);

        backgroundColor = browser.findElement(By.xpath("//*[@id=\"d-pollView\"]/article/div/div/div/ul/li"));
        System.out.println(backgroundColor.getCssValue("background-color"));
    }


    @After
    public void closeBrowser(){
        browser.close();
        browser.quit();
    }

    @AfterClass
    public static void quitBrowser(){
        System.out.println("Finished execution of all test cases.");
    }
}
