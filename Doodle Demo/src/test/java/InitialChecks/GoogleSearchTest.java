package InitialChecks;

import org.junit.*;
import Browser.Browser;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GoogleSearchTest {
    private static Browser browser;
    String googleHomepageURL = "https://www.google.com/";

    @BeforeClass
    public static void initBrowser(){
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        browser = new Browser();
    }

    @Test
    public void test_01_openGoogleHomepage() throws InterruptedException {
        browser.goTo(googleHomepageURL);
        WebElement googleLogo = browser.findElement(By.xpath("//*[@id=\"hplogo\"]"));
        boolean shown = googleLogo.isDisplayed();
        try {
            Assert.assertEquals(true, shown);
        }
        catch (AssertionError ae) {
            System.out.println("Google Logo is not visible.");
            throw ae;
        }
        System.out.println("Google Logo is correctly displayed on Google Homepage.");
    }

    @Test
    public void test_02_googleSearchDoodle() throws InterruptedException{

        browser.goTo(googleHomepageURL);
        Assert.assertEquals(true, browser.isElementDisplayed(By.xpath("//*[@id=\"hplogo\"]")));
        Assert.assertEquals(true, browser.isElementDisplayed(By.xpath("//*[@id=\"lst-ib\"]")));
        browser.sendKeys(By.xpath("//*[@id=\"lst-ib\"]"), "Doodle.com", true);

        Assert.assertEquals(true, browser.waitElementDisplayed(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/h3/a"), 10));
        Assert.assertEquals("Doodle", browser.getText(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/h3/a")));
        browser.click(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/h3/a"));

        boolean shown = browser.waitElementDisplayed(By.xpath("//*[@id=\"content\"]/div[2]/div/div[1]/h1"), 10);
        Assert.assertEquals(true, shown);
        Assert.assertEquals("Get together with Doodle", browser.getText(By.xpath("//*[@id=\"content\"]/div[2]/div/div[1]/h1")));
        System.out.println("Get together with Doodle is successfully shown on Doodle.com webpage");
    }

    @AfterClass
    public static void closeBrowser(){
        browser.close();
        browser.quit();
    }
}
