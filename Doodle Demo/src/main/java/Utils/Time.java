package Utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.*;

public class Time {
    private LocalDate date;

    public Time(){
        date = LocalDate.now();
    }

    public String getTodaysDay(){
        DayOfWeek dow = date.getDayOfWeek();
        String dayName = dow.getDisplayName(TextStyle.FULL, Locale.ENGLISH);
        System.out.println(dayName);
        return dayName;
    }

    public String getTodaysDate(){
        System.out.println(date.toString());
        return date.toString();
    }

    public int getRemainingNumberOfDays(){
        return date.lengthOfMonth() - Integer.parseInt(date.toString().split("-")[2])+1;
    }

    public ArrayList<String> getDatesUntilEndOfMonth(){
        int daysInMonth = date.lengthOfMonth();
        int remainingDays = daysInMonth - Integer.parseInt(date.toString().split("-")[2])+1;
        ArrayList<String> dates = new ArrayList<String>(remainingDays);
        for (int i=0; i<remainingDays;i++){
            dates.add(date.toString().replace(date.toString().split("-")[2], Integer.toString(Integer.parseInt(date.toString().split("-")[2])+i)));
        }
        //System.out.println(Arrays.toString(dates.toArray()));
        return dates;
    }

    public ArrayList<String> getRandomDaysUntilEndOfMonth(int neededDays){
        int daysInMonth = date.lengthOfMonth();
        int remainingDays = daysInMonth - Integer.parseInt(date.toString().split("-")[2])+1;
        ArrayList<String> dates = new ArrayList<String>(remainingDays);
        for (int i=0; i<remainingDays;i++){
            dates.add(date.toString().replace(date.toString().split("-")[2], Integer.toString(Integer.parseInt(date.toString().split("-")[2])+i)));
        }

        if (neededDays >= remainingDays) {
            return dates;
        }
        else {
            ArrayList<String> neededDates = new ArrayList<String>(neededDays);

            for (int i=0; i<neededDays; i++){
                Collections.shuffle(dates);
                neededDates.add(dates.get(0));
                dates.remove(0);
                //System.out.println(Arrays.toString(dates.toArray()));
            }
            //System.out.println(Arrays.toString(neededDates.toArray()));
            return neededDates;
        }
    }
}
