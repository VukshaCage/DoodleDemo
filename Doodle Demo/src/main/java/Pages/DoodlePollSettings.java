package Pages;

import Browser.Browser;
import org.openqa.selenium.By;

public class DoodlePollSettings {
    private Browser browser;

    //Paths
    String pollSettings = "//*[@id=\"d-wizardSettingsView\"]/div/header/h2";
    String yesNoMaybeComponent = "//*[@id=\"d-ifneedbeCheckboxContainer\"]";
    String yesNoMaybeCheckbox = "//*[@id=\"d-ifneedbeCheckbox\"]";
    String continueButton = "//*[@id=\"d-wizardSettingsNavigationView\"]/div/div/div[2]/button[2]";

    public DoodlePollSettings(Browser browser){
        this.browser = browser;
    }

    public boolean enteredPollSettingsPage() throws InterruptedException {
        return browser.waitElementDisplayed(By.xpath(pollSettings), 10);
    }

    public String getPollSettings(){
        return browser.getText(By.xpath(pollSettings));
    }

    public String getYesNoMaybe(){
        return browser.getText(By.xpath(yesNoMaybeComponent + "/div/div[2]/div"));
    }

    public void selectPollOptions(boolean yesNoMaybe, boolean limitNumber, boolean limitParticipants) throws Exception{
        if (yesNoMaybe){
            if (!browser.isCheckboxSelected(By.xpath(yesNoMaybeCheckbox))){
                browser.click(By.xpath(yesNoMaybeComponent));
                if (!browser.isCheckboxSelected(By.xpath(yesNoMaybeCheckbox))){
                    throw new Exception("Element has not been selected.");
                }
            }
        }
        if (limitNumber){
            //ToDo: Not needed now.
        }
        if (limitParticipants){
            //ToDo: Not needed now.
        }
    }

    public boolean getContinueButtonStatus() {
        return browser.isElementEnabled(By.xpath(continueButton));
    }

    public void clickOnContinue() {
        browser.click(By.xpath(continueButton));
    }
}
