package Pages;

import Browser.Browser;
import org.openqa.selenium.By;

public class DoodleIntroduceToParticipants {
    private Browser browser;

    //Paths
    String participantsHeader = "//*[@id=\"d-wizardInitiatorView\"]/header/h2";
    String nameInput = "//*[@id=\"d-initiatorName\"]";
    String emailInput = "//*[@id=\"d-initiatorEmail\"]";
    String continueButton = "//*[@id=\"d-persistPollButton\"]";

    public DoodleIntroduceToParticipants(Browser browser){
        this.browser = browser;
    }

    public boolean enteredIntroduce() throws InterruptedException {
        return browser.waitElementDisplayed(By.xpath(participantsHeader), 10);
    }

    public String getIntroduce(){
        return browser.getText(By.xpath(participantsHeader));
    }

    public void enterUserData(String name, String email){
        if (!name.isEmpty()){
            browser.sendKeys(By.xpath(nameInput), name, false);
        }
        if (!email.isEmpty()){
            browser.sendKeys(By.xpath(emailInput), email, false);
        }
    }

    public boolean getContinueButtonStatus() {
        return browser.isElementEnabled(By.xpath(continueButton));
    }

    public void clickOnContinue() {
        browser.click(By.xpath(continueButton));
    }

    public String buttonContinueText() {
        return browser.getText(By.xpath(continueButton + "/div/div[1]/div/div"));
    }

}
