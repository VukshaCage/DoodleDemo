package Pages;

import Browser.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class DoodleOptionCalendar {
    private Browser browser;

    //Paths
    String whatAreTheOptions = "//*[@id=\"d-wizardOptionsPage\"]/div/div/div/header[2]/h2";
    String optionHolder = "//*[@id=\"d-monthWeekTabSwitchView\"]/section/ul";
    String addTimesButton = "//*[@id=\"d-wizardDatetimePickerView\"]/div/div/button[1]/div/div[1]/div[2]/div";
    String continueButton = "//*[@id=\"d-wizardOptionsNavigationView\"]/div/div/div[2]/button[2]";
    String inputTimesLine = "//*[@id=\"d-wizardDatetimePickerView\"]/div/div[1]/div/div/ul/li/div";

    public DoodleOptionCalendar(Browser browser) {
        this.browser = browser;
    }

    public boolean enteredCalendarPage() throws InterruptedException {
        return browser.waitElementDisplayed(By.xpath(whatAreTheOptions), 10);
    }

    public String getOptions(){
        return browser.getText(By.xpath(whatAreTheOptions));
    }

    public String SelectOption(String option) throws Exception{
        WebElement activeOption;
        switch (option){
            case "Month":
                browser.waitElementDisplayed(By.xpath(optionHolder + "/li[1]/a"), 5);
                activeOption = browser.findElement(By.xpath(optionHolder + "/li[1]/a"));
                browser.click(activeOption);
                if (!activeOption.getAttribute("class").equals("d-activeTab")){
                    throw new Exception("Option is still not selected, even after clicking on it.");
                }
                return browser.getText(activeOption);
            case "Week":
                browser.waitElementDisplayed(By.xpath(optionHolder + "/li[2]/a"), 5);
                activeOption = browser.findElement(By.xpath(optionHolder + "/li[2]/a"));
                browser.click(activeOption);
                if (!activeOption.getAttribute("class").equals("d-activeTab")){
                    throw new Exception("Option is still not selected, even after clicking on it.");
                }
                return browser.getText(activeOption);
            case "Text":
                browser.waitElementDisplayed(By.xpath(optionHolder + "/li[3]/a"), 5);
                activeOption = browser.findElement(By.xpath(optionHolder + "/li[3]/a"));
                browser.click(activeOption);
                if (!activeOption.getAttribute("class").equals("d-activeTab")){
                    throw new Exception("Option is still not selected, even after clicking on it.");
                }
                return browser.getText(activeOption);
            default: throw new Exception("There is no such option available on this page.");
        }
    }

    public void selectCalendarDates(ArrayList<String> dates, boolean addTimes, String startTime, String endTime) throws Exception{
        int sizeDate = dates.size();
        WebElement dayToSelect;
        for (int i=0; i<sizeDate; i++){
            dayToSelect = browser.findElement(By.xpath("//*[@data-date='" + dates.get(i) + "']"));
            if (!dayToSelect.getText().equals(dates.get(i).split("-")[2])){
                throw new Exception("Day displayed on button does not correspond to selected calendar Date.");
            }
            browser.click(dayToSelect);
            dayToSelect = browser.findElement(By.xpath("//*[@data-date='" + dates.get(i) + "']//div"));
            if (!dayToSelect.getAttribute("class").contains("d-selected")){
                throw new Exception("Element has not been selected.");
            }
        }
        if (addTimes) {
            browser.click(By.xpath(addTimesButton));

            browser.click(By.xpath(inputTimesLine + "/div[1]/div/div/input"));
            browser.sleep();
            browser.sendKeys(By.xpath("//*[@id=\"d-timePickerInput\"]"), startTime, false);
            browser.click(By.xpath("//*[@id=\"d-doneButton\"]"));

            browser.click(By.xpath(inputTimesLine + "/div[2]/div/div/input"));
            browser.sleep();
            browser.sendKeys(By.xpath("//*[@id=\"d-timePickerInput\"]"), endTime, false);
            browser.click(By.xpath("//*[@id=\"d-doneButton\"]"));
        }
    }

    public boolean getContinueButtonStatus() {
        return browser.isElementEnabled(By.xpath(continueButton));
    }

    public void clickOnContinue() {
        browser.click(By.xpath(continueButton));
    }
}
