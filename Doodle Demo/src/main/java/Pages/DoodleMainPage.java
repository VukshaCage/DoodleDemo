package Pages;

import Browser.Browser;
import org.openqa.selenium.By;

public class DoodleMainPage {
    private Browser browser;

    //Paths
    String doodleHomepageURL = "https://doodle.com/";
    String doodleWelcomeMessage = "//*[@id=\"content\"]/div[2]/div/div[1]/h1";
    String doodlePollButton = "//*[@id=\"content\"]/div[2]/div/form/button";

    public DoodleMainPage(Browser browser){
        this.browser = browser;
    }

    public void enterDoodleMainPage() throws InterruptedException{
        browser.goTo(doodleHomepageURL);
    }

    public String getDoodleWelcomeMessage(){
        return browser.getText(By.xpath(doodleWelcomeMessage));
    }

    public void clickOnDoodlePollButton(){
        browser.click(By.xpath(doodlePollButton));
    }
}
