package Pages;

import Browser.Browser;
import org.openqa.selenium.By;

public class DoodlePollPage {
    private Browser browser;

    //Paths
    static String urlPath = "";
    static String columnPath = "//*[@id=\"d-pollView\"]/article/div/div/div/ul/li[1]";
    static String firstCheckboxClass = columnPath + "/label/div[3]";
    //static String firstCheckboxPath = "//*[@class=\"d-defaultCheckmark\"]"; /** Not that good of implementation because this works only because I am after first checkbox element, would not work for any others.*/

    public DoodlePollPage(Browser browser){
        this.browser = browser;
    }

    public void setUrlPath(){
        urlPath = browser.findElement(By.xpath("//*[@id=\"d-pollLink\"]")).getAttribute("value");
    }

    public String getUrlPath(){
        return urlPath;
    }

    public String checkClick() throws Exception {
        String rgba;
        browser.setMouse(By.xpath(firstCheckboxClass));
        if (!browser.waitElementDisplayed(browser.findElement(By.xpath(firstCheckboxClass)), 10)){
            throw new Exception("Checkbox is not visible after 10 seconds and cannot be clickable.");
        }
        browser.click(By.xpath(firstCheckboxClass));
        browser.sleep();
        rgba = browser.findElement(By.xpath(columnPath)).getCssValue("background-color");
        if (!browser.findElement(By.xpath(firstCheckboxClass)).getAttribute("class").equalsIgnoreCase("d-checkbox d-participantPreference d-yesPreference")){
            throw new Exception("Checkbox is not marked as a \"Yes\".");
        }
        return parseRGBaToHex(rgba);
    }

    public String checkDoubleClick() throws Exception {
        String rgba;
        browser.setMouse(By.xpath(firstCheckboxClass));
        if (!browser.waitElementDisplayed(browser.findElement(By.xpath(firstCheckboxClass)), 10)){
            throw new Exception("Checkbox is not visible after 10 seconds and cannot be clickable.");
        }
        browser.click(By.xpath(firstCheckboxClass));
        browser.click(By.xpath(firstCheckboxClass));
        browser.sleep();
        rgba = browser.findElement(By.xpath(columnPath)).getCssValue("background-color");
        if (!browser.findElement(By.xpath(firstCheckboxClass)).getAttribute("class").equalsIgnoreCase("d-checkbox d-participantPreference d-ifneedbePreference")){
            throw new Exception("Checkbox is not marked as a \"Yes, if need be\".");
        }
        return parseRGBaToHex(rgba);
    }

    public String parseRGBaToHex(String rgba) throws Exception {
        String hex = "#";
        rgba = rgba.replaceAll("rgba", "").replaceAll(" ", "").replaceAll("\\(", "").replaceAll("\\)", "");
        hex += Integer.toHexString(Integer.parseInt(rgba.split(",")[0]));
        hex += Integer.toHexString(Integer.parseInt(rgba.split(",")[1]));
        hex += Integer.toHexString(Integer.parseInt(rgba.split(",")[2]));
        //hex += Integer.toHexString(Integer.parseInt(rgba.split(",")[3])); - Checking without "a" parameter.
        return hex.toUpperCase();
    }
}
