package Pages;

import Browser.Browser;
import org.openqa.selenium.By;

public class DoodlePollOccasionPage {
    private Browser browser;

    //Paths
    String whatIsOccassion = "//*[@id=\"d-wizardGeneralInformationView\"]/header/h2";
    String titlePath = "//*[@id=\"d-pollTitle\"]";
    String locationPath = "//*[@id=\"d-pollLocation\"]";
    String notePath = "//*[@id=\"d-pollDescription\"]";
    String continueButtonPath = "//*[@id=\"d-wizardGeneralInformationNavigationView\"]/div/div/div[2]/button";


    public DoodlePollOccasionPage(Browser browser){
        this.browser = browser;
    }

    public boolean enteredPollOccassionPage() throws InterruptedException {
        return browser.waitElementDisplayed(By.xpath(whatIsOccassion), 10);
    }

    public String getWhatIsOccassion(){
        return browser.getText(By.xpath(whatIsOccassion));
    }

    public boolean checkInitOccassionForm() throws InterruptedException{
        boolean expected = true;
        expected &= browser.waitElementDisplayed(By.xpath(titlePath), 1);
        expected &= browser.waitElementDisplayed(By.xpath(locationPath), 1);
        expected &= browser.waitElementDisplayed(By.xpath(notePath), 1);
        expected &= browser.waitElementDisplayed(By.xpath(continueButtonPath), 1);

        return expected;
    }

    public void fillOccassionForm(String title, String location, String... notes) throws Exception {
        browser.sendKeys(By.xpath(titlePath), title, false);
        browser.sendKeys(By.xpath(locationPath), location, true);
        for (int i=0; i<notes.length-1;i++){
            browser.sendKeys(By.xpath(notePath), notes[i], true);
            if (i==(notes.length-1)){
                browser.sendKeys(By.xpath(notePath), notes[i], false);
            }
        }
    }

    public boolean getContinueButtonStatus() {
        return browser.isElementEnabled(By.xpath(continueButtonPath));
    }

    public void clickOnContinue(){
        browser.click(By.xpath(continueButtonPath));
    }
}
