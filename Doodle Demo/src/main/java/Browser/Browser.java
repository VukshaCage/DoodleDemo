package Browser;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.nio.charset.Charset;

public class Browser {

    private static RemoteWebDriver driver;
    private static final double timeIncCounter = 0.5;

    /** BASE ACTIONS */
    public Browser(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
    }

    public void sleep(double ammount) throws InterruptedException {
        Thread.sleep(Math.round(1000*ammount));
    }

    public void sleep() throws InterruptedException {
        Thread.sleep(Math.round(1000*timeIncCounter));
    }

    public void close() {
        driver.close();
    }

    public void quit() {
        driver.quit();
    }

    public void leave() {
        driver.switchTo().alert().accept();
    }

    public void goTo(String address) throws InterruptedException {
        System.out.println("Going to address: " + address);
        driver.get(address);
        Thread.sleep(Math.round(2*1000*timeIncCounter));
    }

    /** WebElement ACTIONS */
    public WebElement findElement(By search){
        return driver.findElement(search);
    }

    public boolean isElementDisplayed(By search){
        return findElement(search).isDisplayed();
    }

    public boolean waitElementDisplayed(By search, double waitTime) throws InterruptedException{
        boolean displayed = false;
        double counter = 0;
        WebElement we;
        while (!displayed && counter<=waitTime){
            we = findElement(search);
            if (we.isDisplayed()) {
                displayed = true;
            }
            else {
                Thread.sleep(Math.round(1000*timeIncCounter));
                counter += timeIncCounter;
            }
        }
        return displayed;
    }

    public boolean waitElementDisplayed(WebElement element, double waitTime) throws InterruptedException{
        boolean displayed = false;
        double counter = 0;
        WebElement we;
        while (!displayed && counter<=waitTime){
            if (element.isDisplayed()) {
                displayed = true;
            }
            else {
                Thread.sleep(Math.round(1000*timeIncCounter));
                counter += timeIncCounter;
            }
        }
        return displayed;
    }

    public boolean isElementEnabled(By search) {
        return findElement(search).isEnabled();
    }

    public boolean isCheckboxSelected(By search) {
        return findElement(search).isSelected();
    }

    public void click(By search){
        findElement(search).click();
    }

    public void click(WebElement we){
        we.click();
    }

    public void doubleClick(By search){
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(search)).doubleClick().build().perform();
    }

    public void setMouse(By search){
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(search)).perform();
    }

    public void sendKeys(By search, String keys, boolean enter){
        WebElement we = findElement(search);
        we.sendKeys(keys);
        if (enter){
            we.sendKeys(Keys.ENTER);
        }
    }

    public String getText(By search){
        return new String(findElement(search).getText().getBytes(Charset.forName("utf-8")));
    }

    public String getText(WebElement we){
        return new String(we.getText().getBytes(Charset.forName("utf-8")));
    }
}
